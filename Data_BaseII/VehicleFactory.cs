﻿namespace DataBaseII
{
    public static class VehicleFactory
    {
        public static Vehicle FromString(string line)
        {
            string[] words = line.Split(' ');
            string subline = line.Substring(line.IndexOf(' ') + 1);
            string type = words[0];

            if (type == typeof(Car).Name)
            {
                return new Car("N/A", "N/A").FromString(subline);
            }
            else if (type == typeof(SportCar).Name)
            {
                return new SportCar("N/A", "N/A", 0).FromString(subline);
            }
            else if (type == typeof(Bike).Name)
            {
                return new Bike("N/A", 0).FromString(subline);
            }
            else if (type == typeof(Bicycle).Name)
            {
                return new Bicycle("N/A", 0, "N/A").FromString(subline);
            }
            else if (type == typeof(Scooter).Name)
            {
                return new Scooter("N/A", 0, 0).FromString(subline);
            }
            else
            {
                return null;
            }
        }
    }
}
