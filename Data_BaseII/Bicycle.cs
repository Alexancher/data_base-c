﻿namespace DataBaseII
{
    public class Bicycle : Bike
    {
        public Bicycle(string name, int wheels, string engine)
            : base(name, wheels)
        {
            Engine = engine;
        }

        public string Engine { get; set; }

        public override string ToString()
        {
            return base.ToString() + " Engine = " + Engine;
        }

        public override Vehicle FromString(string line)
        {
            Bicycle newBicycle = (Bicycle)base.FromString(line);
            newBicycle.Engine = line.Split(' ')[8];

            return newBicycle;
        }
    }
}
