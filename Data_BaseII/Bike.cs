﻿using System;

namespace DataBaseII
{
    public class Bike : Vehicle
    {
        public Bike(string name, int wheels)
            : base(name)
        {
            Wheels = wheels;
        }

        public int Wheels { get; set; }

        public override string ToString()
        {
            return base.ToString() + " Wheels = " + Wheels.ToString();
        }

        public override Vehicle FromString(string line)
        {
            Bike newBike = (Bike)base.FromString(line);
            newBike.Wheels = Int32.Parse(line.Split(' ')[5]);

            return newBike;
        }
    }
}
