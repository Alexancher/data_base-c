﻿using System;

namespace DataBaseII
{
    public class Scooter : Vehicle
    {
        public Scooter(string name, double size, double price)
            : base(name)
        {
            Size = size;
            Price = price;
        }

        public double Size { get; set; }

        public double Price { get; set; }

        public override string ToString()
        {
            return base.ToString() + " Size = " + Size.ToString() + " Price = " + Price.ToString();
        }

        public override Vehicle FromString(string line)
        {
            Scooter newScooter = (Scooter)base.FromString(line);
            string[] words = line.Split(' ');
            newScooter.Size = Double.Parse(words[5]);
            newScooter.Price = Double.Parse(words[8]);

            return newScooter;
        }
    }
}
