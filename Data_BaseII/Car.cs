﻿namespace DataBaseII
{
    public class Car : Vehicle
    {
        public Car(string name, string color) : base(name)
        {
            Color = color;
        }

        public string Color { get; set; }

        public override string ToString()
        {
            return base.ToString() + " Color = " + Color;
        }

        public override Vehicle FromString(string line)
        {
            Car newCar = (Car)base.FromString(line);
            newCar.Color = line.Split(' ')[5];

            return newCar;
        }
    }
}
