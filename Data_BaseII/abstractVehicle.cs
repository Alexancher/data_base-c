﻿namespace DataBaseII
{
    public abstract class Vehicle
    {
        public Vehicle(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public int ID { get; set; }

        public override string ToString()
        {
            return this.GetType().Name + " " + "Name = " + Name;
        }

        public virtual Vehicle FromString(string line)
        {
            this.Name = line.Split(' ')[2];

            return this;
        }
    }
}
