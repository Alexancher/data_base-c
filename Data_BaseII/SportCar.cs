﻿using System;

namespace DataBaseII
{
    public class SportCar : Car
    {
        public SportCar(string name, string color, double topSpeed)
            : base(name, color)
        {
            TopSpeed = topSpeed;
        }

        public double TopSpeed { get; set; }

        public override string ToString()
        {
            return base.ToString() + " TopSpeed = " + TopSpeed.ToString();
        }

        public override Vehicle FromString(string line)
        {
            SportCar newSportCar = (SportCar)base.FromString(line);
            newSportCar.TopSpeed = Double.Parse(line.Split(' ')[8]);

            return newSportCar;
        }
    }
}
