﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

namespace DataBaseII
{
    public class DataBase
    {
        private int NextID = 0;
        private List<Vehicle> Container = new List<Vehicle>();
        public string Name { get; set; }
        public int Size { 
            get 
            { 
                return this.Container.Count; 
            } 
        }
        public string Path
        {
            get
            {
                return Name + ".txt";
            }
        }
        
        public DataBase(string name)
        {
            Name = name;
            Load();
        }

        public string ShowBase()
        {
            var builder = new StringBuilder(NextID.ToString() + '\n');

            foreach (var element in Container)
            {
                builder.Append(element.ID.ToString() + ' ' + element.ToString() + '\n');
            }

            return builder.ToString();
        }

        public void Add(Vehicle element)
        {
            if (element != null)
            {
                element.ID = NextID++;
                Container.Add(element);
            }
        }

        public Vehicle Find(int id)
        {
            foreach (var element in Container)
            {
                if (element.ID == id)
                {
                    return element;
                }
            }

            return null;
        }

        public bool Remove(int id)
        {
            if (Find(id) != null)
            {
                Container.Remove(Find(id));
                return true;
            }

            return false;
        }

        public void Clear()
        {
            Container.Clear();    
        }

        public void Save()
        {
            using (var writer = File.CreateText(Path))
            {
                writer.WriteLine(ShowBase());
            }
        }

        private void Load()
        {
            if (File.Exists(Path))
            {
                using (var Reader = new StreamReader(Path))
                {
                    try
                    {
                        var line = Reader.ReadLine();
                        var newNextID = 0;

                        if (!string.IsNullOrEmpty(line))
                        {
                            newNextID = int.Parse(line);
                        }

                        while (!string.IsNullOrEmpty(line))
                        {
                            NextID = int.Parse(line.Split(' ')[0]);
                            var subline = line.Substring(line.IndexOf(' ') + 1);
                            Add(VehicleFactory.FromString(subline));
                            line = Reader.ReadLine();
                        }
                        NextID = newNextID;
                    }
                    catch
                    {
                        throw new FormatException("Invalid file format");
                    }
                }
            }
        }
    }
}
