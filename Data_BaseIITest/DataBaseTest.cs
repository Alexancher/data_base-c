﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataBaseII;

namespace Data_BaseIITest
{
    [TestClass]
    public class DataBaseTest
    {
        [TestMethod]
        public void AddTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");

            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            Assert.AreEqual(1, testBase.Size, "Failed while adding a Car");

            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));
            Assert.AreEqual(2, testBase.Size, "Failed while adding a SportCar");

            testBase.Add(VehicleFactory.FromString("Bike Name = testBike Wheels = 42"));
            Assert.AreEqual(3, testBase.Size, "Failed while adding a Bike");

            testBase.Add(VehicleFactory.FromString(("Bicycle Name = testBicycle Wheels = 42 Engine = testEngine")));
            Assert.AreEqual(4, testBase.Size, "Failed while adding a Bicycle");

            testBase.Add(VehicleFactory.FromString("Scooter Name = testScooter Price = 42 Size = 42"));
            Assert.AreEqual(5, testBase.Size, "Failed while adding a Scooter");
        }

        [TestMethod]
        public void RemoveTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");
            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));
            testBase.Add(VehicleFactory.FromString("Bike Name = testBike Wheels = 42"));
            testBase.Add(VehicleFactory.FromString(("Bicycle Name = testBicycle Wheels = 42 Engine = testEngine")));
            testBase.Add(VehicleFactory.FromString("Scooter Name = testScooter Price = 42 Size = 42"));

            testBase.Remove(0);
            Assert.AreEqual(4, testBase.Size, "Failed while removing the first element");

            testBase.Remove(4);
            Assert.AreEqual(3, testBase.Size, "Failed while removing the last elemrnt");

            testBase.Remove(2);
            Assert.AreEqual(2, testBase.Size, "Failed while removing mid element");
        }

        [TestMethod]
        public void ShowBaseTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");
            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));

            string expected = "2\n0 Car Name = testCar Color = testColor\n1 SportCar Name = testSportCar Color = testColor TopSpeed = 42\n";
            Assert.AreEqual(expected, testBase.ShowBase(), "ShowBase does not work correctly");
        }

        [TestMethod]
        public void SaveTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");
            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));

            testBase.Save();
            Assert.AreEqual(true, File.Exists(testBase.Name + ".txt"), "Failed while saving. File does not exist");      
        }

        [TestMethod]
        public void LoadTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");
            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));
            testBase.Save();

            DataBase newBase = new DataBase("testBase");
            Assert.AreEqual(testBase.ToString(), newBase.ToString(), "Failed while loading. Bases are not equal");
        }

        [TestMethod]
        public void FindTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");
            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));
            testBase.Add(VehicleFactory.FromString("Bike Name = testBike Wheels = 42"));

            Assert.AreEqual("Car Name = testCar Color = testColor", testBase.Find(0).ToString(), "Did not found first element correctly");
            Assert.AreEqual("SportCar Name = testSportCar Color = testColor TopSpeed = 42", testBase.Find(1).ToString(), "Did not found mid element correctly");
            Assert.AreEqual("Bike Name = testBike Wheels = 42", testBase.Find(2).ToString(), "Did not found last element correctly");
        }

        [TestMethod]
        public void ClearTest()
        {
            File.Delete("testBase.txt");
            DataBase testBase = new DataBase("testBase");
            testBase.Add(VehicleFactory.FromString("Car Name = testCar Color = testColor"));
            testBase.Add(VehicleFactory.FromString("SportCar Name = testSportCar Color = testColor TopSpeed = 42"));

            testBase.Clear();
            Assert.AreEqual(0, testBase.Size, "Base is not empty");
        }
    }
}
